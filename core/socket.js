
//const Instance = require('../models/Instance')

let ws = {
    io: null,
    clients: [],
    init: function(io){
        ws.io = io
        // Quand un client se connecte, on le note dans la console
        ws.io.sockets.on('connect', function (socket) {
            console.log('Un client est connecté !', ws.clients.length);
            socket.emit("conf-connexion", { msg: "hello" })

            socket.on('join', (data) => {
                //delete older socket if exists
                ws.clients.forEach((c,i)=>{ if(c.UID == data.UID) ws.clients.splice(i, 1) })
                //push new socket
                ws.clients.push({ UID: data.UID, socket : socket })
                console.log('JOIN !', data.UID);
            });

            socket.on('leave', (data) => {
                console.log('LEAVE !', data.UID);
            });


        });
    },
    emit: function(destUID, action, data){
        console.log("ws.clients", ws.clients.length, destUID)
        let sock = null
        ws.clients.forEach((c,i)=>{ console.log("c.UID", c.UID)
            if(c.UID == destUID) sock = c.socket 
        })

        if(sock != null){
            sock.emit(action, data)
            console.log("socket send to", destUID, action, data)
        }else{
            console.log("error : socket receiver unknown", destUID, ws.clients.length)
        }
    },
    /*iName: function(uid){
        let i = uid.indexOf(':')
        return uid.substr(0, i)
    },
    iKey: function(uid){
        let i = uid.indexOf(':')
        return uid.substr(i+1, uid.length)
    },*/
}
module.exports = ws;