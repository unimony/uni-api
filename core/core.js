/*var express = require("express")
var router = express.Router()
var app = express()

const auth = require("../middleware/auth-anonymous");
const bcrypt = require("bcrypt");


const jwt = require('jsonwebtoken');
const axios = require('axios');
const config = require('config');
*/
const { User, validate } = require('../models/User')
const Wallet = require('../models/Wallet')
const Instance = require('../models/Instance')
const StateHistory = require('../models/StateHistory')
const Transaction = require('../models/Transaction')
const axios = require('axios');



let core = {
    iName: function(uid){
        let i = uid.indexOf(':')
        return uid.substr(0, i)
    },
    iKey: function(uid){
        let i = uid.indexOf(':')
        return uid.substr(i+1, uid.length)
    },

    broadcaster : {

        init: function(){
            axios.interceptors.request.use(
                async (config) => {
                let i = await Instance.findOne({ isLocal: true })
                let token = i.name + ':' + i.nodeKey
                console.log("XXXXX init core - token:", token)
                if (token) {
                    config.headers['x-auth-token'] = token
                }
                return config;
                }, 
            
                (error) => {
                return Promise.reject(error)
                }
            );
        },
        
        get : async function(urlAction, fThen, fCatch, concat){
            core.broadcaster.init()
            //get info about all instances in the network
            const network = await Instance.find() //{'isLocal': false}) 
            const nbNode = network.length
            //init array for response datas
            let dataRes = []
            //init counter of response from nodes
            let nbNodeOk = 0

            network.forEach(node => {
                console.log("send request to", node.url+":"+node.port)
                //send request to each node in the network
                axios.get(node.url+":"+node.port + urlAction)
                    .then(result => {
                        //incremment counter of response
                        nbNodeOk++
                        //for each response
                        if(result.data.error != true){
                            //if params concat is not null && there are data in response : 
                            //concat data reponse to finally send all in one array
                            if(concat != null && result.data[concat] != null)
                                dataRes = dataRes.concat(result.data[concat])
                            console.log("nbNodeOk", nbNodeOk, "nbNode", nbNode)
                            //if it is the last responses
                            if(nbNodeOk == nbNode){  fThen(dataRes) } //exec fThen function from parameters
                        }else{
                            //if response send an error
                            fCatch(result) //exec fCatch function from parameters
                        }

                    }).catch(result => { //if request failed
                        //incremment counter of response
                        nbNodeOk++
                        console.log("error request", node.url+":"+node.port, "error ?", result)
                        fCatch(result) //exec fCatch function from parameters
                    })
            })
            //if no instance in the network
            if(network.length==0) 
                res.json({  error: true,
                            errorMsg: 'This network is empty' })
        },
    

        //this function should works but has not been tried
        post : async function(urlAction, postParams, fThen, fCatch, concat){
            core.broadcaster.init()
            //get info about all instances in the network
            const network = await Instance.find() //{'isLocal': false}) 
            const nbNode = network.length
            //init array for response datas
            let dataRes = []
            //init counter of response from nodes
            let nbNodeOk = 0

            network.forEach(node => {
                console.log("send request to", node.url+":"+node.port)
                //send request to each node in the network
                axios.post(node.url+":"+node.port + urlAction, postParams)
                    .then(result => {
                        //incremment counter of response
                        nbNodeOk++
                        //for each response
                        if(result.data.error != true){
                            //if params concat is not null : concat data reponse to finally send all in one array
                            if(concat != null)
                                dataRes = dataRes.concat(result.data[concat])
                            console.log("nbNodeOk", nbNodeOk, "nbNode", nbNode)
                            //if it is the last responses
                            if(nbNodeOk == nbNode){  fThen(dataRes) } //exec fThen function from parameters
                        }else{
                            //if response send an error
                            fCatch(result) //exec fCatch function from parameters
                        }

                    }).catch(result => { //if request failed
                        console.log("error request", node.url+":"+node.port, "error ?", result)
                        nbNodeOk++
                        fCatch(result) //exec fCatch function from parameters
                    })
            })
            //if no instance in the network
            if(network.length==0) 
                res.json({  error: true,
                            errorMsg: 'This network is empty' })
        } 
    },

    get : async function(url, fThen, fCatch){
        core.broadcaster.init()
        axios.get(url).then(res => {
            fThen(res)
        }).catch(res => {
            fCatch(res)
        })
    },

    post : async function(url, postParams, fThen, fCatch){
        core.broadcaster.init()
        axios.post(url, postParams).then(res => {
            fThen(res)
        }).catch(res => {
            fCatch(res)
        })
    }
}
module.exports = core;