# UniMony API installation :

#### - Install mongodb :
This project is base on MongoDB as database, so you need to install it :

[https://docs.mongodb.com/manual/administration/install-community/](https://docs.mongodb.com/manual/administration/install-community/)

> Don't forget to run mongodb in background before to start running API

> Mongodb has been choosen to start this project quickly, but an other solution could be used for the final project, if needed...

---

## - Clone this repository
```
git clone https://gitlab.com/unimony/uni-api.git
```

## - Install application
```
cd uni-api
npm install
```

## - Run API in terminal
```
node app.js --port 3000
```

Then API is running on port 3000.

http://localhost:3000


## - Run the Transaction Queue Worker (TQW)

When a transaction is sent to the API, the transaction is save in a queue, to be validated later by the TQW process.

To run this process, just call this URL :
http://localhost:3000/tqw/run,
and all unvalidated transaction will be processed.

In production, this process should be executed by a cron. 


---

## [Learn more with the WIKI](https://gitlab.com/unimony/uni-client/wikis/Home)