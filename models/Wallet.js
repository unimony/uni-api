var mongoose = require('mongoose');

var walletSchema = new mongoose.Schema({
    uid: String,
    type: String, //current | custom | bank
    name: String,
    isPublic: Boolean,
    account_u: Number, //ù
    account_uni: Number, //dividend
    created: Date,
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    pendingTransactions: [
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Transaction'
      }
    ],
    validatedTransactions: [
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Transaction'
      }
    ],
});

walletSchema.virtual('wallets', {
    ref: 'User',
    localField: '_id',
    foreignField: 'wallets'
});

var Wallet = mongoose.model('Wallet', walletSchema);

module.exports = Wallet;