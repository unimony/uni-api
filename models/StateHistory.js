var mongoose = require('mongoose');

var stateHistorySchema = new mongoose.Schema({
    date: Date,
    moneyMassLocal: Number,
    moneyMassGlobal: Number,
    nbUsersLocal: Number,
    nbUsersGlobal: Number,
    UDValue: Number,
});

var StateHistory = mongoose.model('stateHistory', stateHistorySchema);

module.exports = StateHistory;