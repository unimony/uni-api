var mongoose = require("mongoose")

var instanceSchema = new mongoose.Schema({
    name: String,
    nodeKey: String,
    url: String,
    port: String,
    portSocket: String,
    city: String,
    state: String,
    lat: Number,
    lng: Number,
    clientUrl: String,
    clientPort: String,
    moneyMassLocal: Number,
    moneyMassGlobal: Number,
    nbUserGlobal: Number,
    UDValue: Number,
    TQRunning: Boolean,
    isLocal: Boolean,
})

var Instance = mongoose.model('Instance', instanceSchema)

Instance.calcUniValue = function(mm, nbUser){
    return mm / nbUser / 365.25 * 12
}

module.exports = Instance;
