* Front END - Clientux

> vue create clientux
- select ROUTER in config before to create project

> cd clientux
> vue add vuetify
> npm i axios

---

* Back END - API

> cd FMS
> mkdir API
> cd API
> npm init
> npm install --save express mongoose cors
> npm install --save vue-chartjs
> npm install --save chart.js
** GIT

> git init
> git add *
> git add .gitignore
> git commit -m 'init'
> git remote add origin https://gitlab.com/unimony/uni-client.git
> git pull origin master