var express = require("express")
var mongosse = require("mongoose")
var router = express.Router()
var app = express()


const auth = require("../middleware/auth-admin");
const bcrypt = require("bcrypt");
const { User, validate } = require('../models/User')
const Instance = require('../models/Instance')
const Wallet = require('../models/Wallet')
const Transaction = require('../models/Transaction')

const config = require('config');
const jwt = require('jsonwebtoken');


router.post('/save/:id?', async (req, res) => {
    console.log("-------------------------")
    console.log("/auth/save")
    try{
        // validate the request body first
        const { error } = validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);
    
        //find an existing user
        let user = await User.findOne({ email: req.body.email });
        if (user) return res.status(400).send("User already registered.");
    
        const i = await Instance.findOne({ isLocal:true })
        let nbUsers = await User.countDocuments()

        //init all new wallet on average value
        //to prevent disturbing UNI relative value
        let firstTrans = i.moneyMassGlobal / nbUsers

        //create current wallet (compte courant)
        let wallet = new Wallet({ 
                        uid: '',
                        type: 'current',
                        name: req.body.name,
                        isPublic: true,
                        account_u: firstTrans,
                        account_uni: 0,
                        pendingTransactions: [],
                        validatedTransactions: [],
                        created: new Date()  
                    });
        
        
        //create new user
        user = new User({
            name: req.body.name,
            password: req.body.password,
            email: req.body.email,
            isActive: false,
            isAdmin: false,
            wallets: [ wallet ]
        });

        
        var name = i != null ? i.name : 'temp'
        wallet.uid = name + ":" + wallet.id
        wallet.owner = user.id

        const futurUD = Instance.calcUniValue(i.moneyMassGlobal+firstTrans, nbUsers+1)

        //create init transaction
        const wbank = await Wallet.findOne({ type: 'bank' })
        let transaction = new Transaction({
            amount: firstTrans,
            senderUid: wbank.uid,
            receiverUid: wallet.uid,
            senderName: wbank.name,
            receiverName: wallet.name,
            senderUni:  0,
            receiverUni: wallet.account_u / futurUD,
            created: new Date(),
            validated: new Date(),
            status: 'validated'
        })

        wallet.validatedTransactions.push(transaction)
        transaction.save()


        //encrypt password
        await bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(user.password, salt, async function(err, hash) {
                user.password = await bcrypt.hash(user.password, 10);

                //save user and wallet into BDD
                await wallet.save();
                await user.save();

                i.moneyMassLocal += firstTrans
                i.moneyMassGlobal = i.moneyMassLocal
                i.nbUserGlobal += 1
                i.UDValue = Instance.calcUniValue(i.moneyMassGlobal, i.nbUserGlobal)
                await i.save()

                res.json({ error: false, user: user })
            });
        });
        
    }catch(e){
        console.log("error", e)
        res.json({ error: true, wallets: null })
    }

});

router.post('/login', (req, res) => {
    new Promise((resolve, reject) => { 
        if(req.body.email && req.body.password){
            User.findOne({ 'email' : req.body.email }).then(resolve, reject);
        }else{
            resolve({ error: true })
        }
    }).then(async user => { 
        if(user == null){        
            res.send({  error: true,  errorMsg: 'e-mail not found' })
        }else if(user.error){     
            res.send({  error: true,  errorMsg: 'incorrect login data' })
        }
        else if(user != null){   
            bcrypt.compare(req.body.password, user.password, function(err, bres) {
                if(bres == false){
                    res.send({  error: true, 
                                errorMsg: 'e-mail ok, but incorrect password' })
                }else{
                    console.log("login", user.name)
                    const token = jwt.sign({ _id: user._id }, config.get('access_pk'))
                    user.password = ""
                    res.send({ error: false, token: token, user: user })
                }
            });
                      
        }
    }).then(() => {
        
    }, err => console.log(err));
});

module.exports = router;