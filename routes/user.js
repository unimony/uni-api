var express = require("express")
var router = express.Router()
var app = express()
var cors = require('cors')

const auth = require("../middleware/auth");
const bcrypt = require("bcrypt");
const { User, validate } = require('../models/User')
const Instance = require('../models/Instance')
const Wallet = require('../models/Wallet')
const Transaction = require('../models/Transaction')

const config = require('config');
const jwt = require('jsonwebtoken');

const axios = require('axios');
const core = require('../core/core');

router.get('/profil', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/user/profil")
    const user = await User.findById(req.user._id)
                           .select("-password")
                           .populate("contacts")
                           .populate({
                            path: 'wallets',
                            populate: { 
                                path: 'pendingTransactions' ,
                                
                            }})
                            .populate({
                             path: 'wallets',
                             populate: { 
                                 path: 'validatedTransactions' ,
                                 
                             }})
    

    const url = config.get('instance_url')
    const port = config.get('instance_port')
    const instance = await Instance.findOne({ isLocal:true })
    
    if(user == null){
        res.json({  user: null, 
                    UDValue: instance != null ? instance.UDValue : null
                 }); return;
    }

    user.wallets.forEach((wallet, ix) => {
        var vTrans = wallet.validatedTransactions
        var nvt = []
        vTrans.reverse().forEach((trans, i) => {
            if(i < 100) nvt.push(trans)
        })
        wallet.validatedTransactions = nvt.reverse()
    })

    res.json( { user : user, 
                UDValue: instance != null ? instance.UDValue : null } );
})

router.post('/search-wallet', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/user/search-wallet", 
                "searchStr", req.body.searchStr)
   
    try{ //broadcast request (to each instance of the network)
        core.broadcaster.post('/broadcast/search-wallet', 
            { searchStr : req.body.searchStr }, //params
            function(dataRes){ // == then (after response from all instances)
                //if network sent data
                if(dataRes != null){
                    res.json({  error: false, wallets: dataRes, 
                                search: req.body.searchStr  })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, wallets: null, 
                            errorMsg: 'No wallet found in this bdd' })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch")
                //res.json({ error: true, wallets: null, 
                           //errorMsg: 'An error occured', result: result })
            },  
            'wallets') //dataRes[id] to concat for each response after forward
    }catch(e){
        console.log("error 3")
        res.json({ error: true, wallets: null, e:e })
    }
})



router.post('/createWallet', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/user/createWallet")

    try{
        const user = await User.findById(req.user._id).populate('wallets');
        
        if(req.body.walletName == null || req.body.walletName == ''){
            console.log("error name", req.body.walletName)
            res.json({ error: true });
            return
        }

        const i = await Instance.findOne({ isLocal:true })
        const wbank = await Wallet.findOne({ type: 'bank' })
        
        let wallet = new Wallet({
            uid: '',
            type: 'custom',
            name: req.body.walletName,
            isPublic: req.body.isPublic,
            account_u: 0,
            account_uni: 0,
            owner: user.id,
            pendingTransactions: [],
            validatedTransactions: [],
            created: new Date()  
        })
        wallet.uid = i.name + ":" + wallet.id

        let transaction = new Transaction({
            amount: 0,
            label: 'Wallet initialisation',
            senderUid: wbank.uid,
            receiverUid: wallet.uid,
            senderName: wbank.name,
            receiverName: wallet.name,
            senderUni:  0,
            receiverUni: 0,
            created: new Date(),
            validated: new Date(),
            status: 'validated'
        })

        wallet.validatedTransactions.push(transaction)
        transaction.save()
        wallet.save()
        user.wallets.push(wallet)
        user.save()
        
        res.json({ error: false, wallet: wallet });
    
    }catch(e){
        console.log("error", e)
        res.json({ error: true, wallets: null })
    }
})

router.post('/sendTransaction', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/user/sendTransaction")

    const user = await User.findById(req.user._id)
                            .select("-password")
                            .populate('wallets')
                            .populate('contacts')
    if(user == null){
        res.json({ user: null, missing_access_token: true })
        return
    }
    try{
        let senderW = await Wallet.findOne( { uid: req.body.senderUid })
                                  .populate("pendingTransactions")
        
        //get receiverW from his instance
        let instanceName = core.iName(req.body.receiverUid)
        const receiverInstance = await Instance.findOne({ name: instanceName })
        let receiverBaseUrl = receiverInstance.url + ":" + receiverInstance.port


        const instance = await Instance.findOne({ isLocal:true })
        let transKey = await bcrypt.hash(instance.name, 10)
        console.log("instance.UDValue ?", instance.UDValue)
        console.log("transKey ?", transKey)
        
        let jsonTrans = {
            amount: req.body.amount,
            label: 'transaction label',
            transKey: transKey,
            senderUid: req.body.senderUid,
            receiverUid: req.body.receiverUid,
            senderName: senderW.name,
            receiverName: null,
            senderUni:  senderW.type == "current" ? senderW.account_u / instance.UDValue : senderW.account_uni,
            receiverUni: null,  
            created: new Date(),
            status: 'pending'
        }
        let transaction = new Transaction(jsonTrans)
        await transaction.save()

        //get receiver data from his instance
        let result = await axios.post(receiverBaseUrl + '/broadcast/receiveTransaction', 
                                    { transaction: jsonTrans })

        let receiverW = result.data.wallet
        console.log("receiverW", receiverW)   
        //if error abort process
        if(receiverW.error == true){
            console.log("xxx error in", receiverBaseUrl + '/broadcast/receiveTransaction', e)
            res.json({ error: true, transaction: null })
            return
        }

        //so, if not aborted
        //add receiver data to the transaction
        transaction.receiverUni = parseFloat(result.data.transaction.receiverUni)
        transaction.receiverName = result.data.transaction.receiverName
        
        //add the transaction to sender's pendings transactions
        senderW.pendingTransactions.push(transaction)
        
        //check if this wallet is already present in my contacts
        var alreadyC = false
        user.contacts.forEach(contactWallet => {
            if(contactWallet.uid == receiverW.uid) alreadyC = true
        }) 
        //if not present : add it to contacts
        if(!alreadyC) user.contacts.push(receiverW)

        //finaly save all data modified
        await senderW.save()
        await transaction.save()
        await user.save()

        /* SEND SOCKET NOTIF TO SEND */
        var uid = instance.name + ":" + senderW.owner._id
        console.log("try emit 'sent-transaction-pending' to", uid)
        req.ws.emit(uid, "sent-transaction-pending", 
                    { receiverName: transaction.receiverName,
                      amountUni: transaction.amount })
        console.log("** EMITED sock to", uid)
        /* SEND SOCKET NOTIF TO SEND */

        res.json({ error: false, transaction: transaction });
    
    }catch(e){
        console.log("error", e)
        res.json({ error: true, transaction: null })
    }
})

module.exports = router;