var express = require("express")
var router = express.Router()
var app = express()

const auth = require("../middleware/auth-anonymous");
const bcrypt = require("bcrypt");

const { User, validate } = require('../models/User')
const Wallet = require('../models/Wallet')
const Instance = require('../models/Instance')
const StateHistory = require('../models/StateHistory')
const Transaction = require('../models/Transaction')

const jwt = require('jsonwebtoken');
const axios = require('axios');
const core = require('../core/core');

const config = require('config');


router.get('/exists'/*, auth*/,  async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/exists")
    
    //const url = config.get('instance_url')
    //const port = config.get('instance_port')
    try{
        const instance = await Instance.findOne({ isLocal: true }).select("-nodeKey") // url: url, port: port }) 
        console.log(instance)
        res.json({ error: false, instance: (instance != null) });
    }catch(e){
        console.log("error", e)
        res.json({ error: false, instance: false, 
                   errorMsg: 'No instance found in this server' })
    }
})

router.get('/get-network'/*, auth*/,  async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/get-network")
    
    //const url = config.get('instance_url')
    //const port = config.get('instance_port')
    try{
        const network = await Instance.find().select("-nodeKey")
        //console.log(network)
        res.json({ network: network });
    }catch(e){
        console.log("error", e)
        res.json({ error: true, network: false, 
                   errorMsg: 'No instance found in this server' })
    }
})


router.post('/enter-network'/*, auth*/,  async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/enter-network")
    try{
        console.log("req.body.url:", req.body.instance.url, "port:", req.body.instance.port)
        const instance = await Instance.findOne({ url: req.body.instance.url, port: req.body.instance.port }) // url: url, port: port }) 
        console.log("instance", instance)
        if( instance != null){
            res.json({  error: true,
                        errorMsg: 'This node is already registred on the network' })
        }else{
            const instance = new Instance(req.body.instance)
            instance.isLocal = false
            instance.save()
            
            const me = await Instance.findOne({ isLocal: true }) // url: url, port: port }) 
            res.json({  error: false,
                        nodeKey: me.nodeKey,
                        errorMsg: 'This node is now registred on the network' })
        }
        
    }catch(e){
        console.log("error", e)
        res.json({ error: true, network: false, 
                   errorMsg: 'No instance found in this server' })
    }
})


router.post('/init'/*, auth*/, async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/init")
    try{

        // REMOVE ALL DOCUMENTS
        console.log("remove", await User.countDocuments(), "Users")
        await User.remove()
        console.log("remove", await Wallet.countDocuments(), "Wallets")
        await Wallet.remove()
        console.log("remove", await StateHistory.countDocuments(), "StateHistorys")
        await StateHistory.remove()
        console.log("remove", await Transaction.countDocuments(), "Transactions")
        await Transaction.remove()
        

        //CREATE THE INSTANCE (in bdd)
        console.log("let's create instance, name:", req.body.name)
        await Instance.remove()
        let instanceName = req.body.name != null ? req.body.name : "alpha"

        let nodeKey = await bcrypt.hash(instanceName, 10)
        
        const instance = new Instance({
            name: instanceName.toLowerCase(),
            nodeKey: nodeKey,
            url: req.body.url != null ? req.body.url : config.get('instance_url'),
            port: req.body.port != null ? req.body.port : config.get('instance_port'),
            portSocket: req.body.portSocket != null ? req.body.portSocket : config.get('instance_port_socket'),
            city: req.body.city,
            state: req.body.state,
            lat: req.body.lat,
            lng: req.body.lng,
            clientUrl: req.body.clientUrl,
            clientPort: req.body.clientPort,
            moneyMassLocal: 1,
            moneyMassGlobal: 1,
            nbUserGlobal: 1,
            UDValue: 0,
            TQRunning: false,
            isLocal: true
        })


        //instance.nbUserGlobal = 1
        instance.UDValue = Instance.calcUniValue(1, 1)
        //instance.moneyMassLocal = instance.UDValue
        //instance.moneyMassGlobal = instance.UDValue
        
        await instance.save();

        let firstTrans = instance.moneyMassGlobal / instance.nbUserGlobal
        
        //CREATE FIRST WALLET = BANK
        console.log("let's create the bank wallet")
        let bankWallet = new Wallet({ 
                                uid: '',
                                type: 'bank',
                                name: "DUBank",
                                isPublic: false,
                                account_u: 0,
                                account_uni: 0,
                                pendingTransactions: [],
                                validatedTransactions: [],
                                created: new Date()  
        });
        bankWallet.uid = instance.name + ":" + bankWallet.id
        await bankWallet.save();
       

        //CREATE FIRST ADMIN USER
        console.log("let's create first admin user")
        //CREATE HIS WALLET
        var name = "SuperAdmin"
        console.log("> create wallet", name)
        let adminWallet = new Wallet({ 
                            uid: '',
                            type: 'current',
                            name: name,
                            isPublic: true,
                            account_u: firstTrans, //instance.UDValue,
                            account_uni: 0,
                            pendingTransactions: [],
                            validatedTransactions: [],
                            created: new Date()  
        });
        adminWallet.uid = instance.name + ":" + adminWallet.id
        
        //CREATE ADMIN USER
        console.log("> create user", name)
        let user = new User({
            name: name,
            password: name,
            email: name + "@mail.com",
            isActive: true,
            isAdmin: true,
            wallets: [ adminWallet ]
        });

        //console.log("save owner", user, user.id)
        adminWallet.owner = user.id
        
        const futurUD = Instance.calcUniValue(instance.moneyMassGlobal+firstTrans, 2)
        //CREATE INIST TRANSACTION
        console.log("> create first init transaction for this wallet")
        let transaction = new Transaction({
            amount: firstTrans,
            label: 'Wallet initialisation',
            senderUid: bankWallet.uid,
            receiverUid: adminWallet.uid,
            senderName: bankWallet.name,
            receiverName: adminWallet.name,
            senderUni:  0,
            receiverUni: adminWallet.account_u / futurUD,
            created: new Date(),
            validated: new Date(),
            status: 'validated'
        })    
        adminWallet.validatedTransactions.push(transaction)

        //encrypt password
        await bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(user.password, salt, async function(err, hash) {
                user.password = await bcrypt.hash(user.password, 10)
                //save user and wallet into BDD
                await adminWallet.save()
                await transaction.save()
                await user.save()
            });
        });            

        let rootNode = config.get('root_node')
        core.get(rootNode.url + ":" + rootNode.port + '/broadcast/get-network', 
            function(res){ //then
                console.log("network ?", res.data)
                res.data.network.forEach(node =>{
                    console.log(node.url+":"+node.port != instance.url+":"+instance.port)
                    if(node.url+":"+node.port != instance.url+":"+instance.port){
                        console.log("go enter network", node)
                        console.log("my instance", instance)
                        
                        Instance.findOne({ url: node.url, port: node.port })
                        .then(iExists => {
                            console.log("iExists", iExists)
                            if( iExists == null ){
                                const i = new Instance(node)
                                i.isLocal = false
                                i.save()
                            }else{
                                console.log("this instance is already registred")
                            }

                            axios.post(node.url + ':' + node.port +'/instance/enter-network', {
                                instance: instance 
                            }).then( res => {
                                if(res.data.nodeKey != null){
                                    let n = Instance.findOne({ url: node.url, port: node.port })
                                    n.nodeKey = res.data.nodeKey
                                    n.save()
                                }
                                console.log("success", node.url + ':' + node.port +'/instance/enter-network', res.data)
                            }).catch( res => {
                                console.log("error", node.url + ':' + node.port +'/instance/enter-network', res.data)
                            })
                        })
                        
                    }else{
                        console.log("this instance is already registred")
                    }
                });
            }, function(res){})
     
        
        
        const admin_access_token = jwt.sign({ _id: user._id }, config.get('access_pk'))
        
        console.log(">>> INIT INSTANCE OK")
        res.json({  error: false, 
                    instance: instance, 
                    admin_access_token: admin_access_token 
                })

    }catch(e){
        console.log("error", e)
        res.json({ error: true, instance: null })
    }
})



module.exports = router;