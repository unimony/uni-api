var express = require("express")
var router = express.Router()

const auth = require("../middleware/auth-node");
const bcrypt = require("bcrypt");
const { User, validate } = require('../models/User')
const Instance = require('../models/Instance')
const Wallet = require('../models/Wallet')
const Transaction = require('../models/Transaction')
const StateHistory = require('../models/StateHistory')

const config = require('config');
const jwt = require('jsonwebtoken');

const axios = require('axios');
const core = require('../core/core');

/*
    BROADCAST : this route file contains all routes suseptibles to be requested by others nodes
    All main decentralized processes should be writen in this file
    Authentification is managed by nodeKey, a unique key for each Instance in the network
*/

router.post('/search-wallet', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/search-wallet", 
                "searchStr", req.body.searchStr)
    try{
        let wallets = await Wallet.find({ 
            "name" : new RegExp(".*"+req.body.searchStr.toLowerCase().trim(), "i"),
            "isPublic" : true
        }).limit(100)

        if(wallets != null){
            console.log("send response no forward")
            res.json({  error: false, wallets: wallets, search: req.body.searchStr  })
        }else{
            console.log("send error no forward")
            res.json({ error: true, 
                       errorMsg: 'No wallet found in this bdd',
                       wallets: null })
        }
    }catch(e){
        console.log("error catch")
        res.json({ error: true, wallets: null, e:e })
    }
})


router.get('/get-network'/*, auth*/,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/get-network")

    /* Sécuriser ce process pour empêcher n'importe qui de récupérer ces données
    surtout les nodeKeys des instances */

    try{
        const network = await Instance.find()
        res.json({ network: network });
    }catch(e){
        console.log("error", e)
        res.json({ error: true, network: false, 
                   errorMsg: 'No instance found in this server' })
    }
})


router.get('/instance-info', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/instance-info")
    
    try{
        let instance = await Instance.findOne({ isLocal:true })
        const nbUsers = await User.countDocuments()
        const nbDays = await StateHistory.countDocuments()

        console.log("send response", instance.name)
        res.json({ instance: instance, 
                    nbUsers: nbUsers, 
                    nbDays: nbDays, 
                    env: config.get('env'),
                    error: false });
    }catch(e){
        console.log("error", e)
        res.json({ error: true, instance: null, 
                   errorMsg: 'No instance found in this server' })
    }
})

router.post('/receiveTransaction'/*, auth*/,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/receiveTransaction")
    
    /* NEED TO SECURE : ONLY FOR OTHER NODES - AUTH/node ? */

    try{
        console.log("req.body.transaction:", req.body.transaction)

        const instance = await Instance.findOne({ isLocal: true })
        
        const wallet = await Wallet.findOne({ uid : req.body.transaction.receiverUid } ) 
        console.log("wallet", wallet)
        if( wallet != null){
            let transaction = await Transaction.findOne({ transKey: req.body.transaction.transKey }) 
            console.log("transaction", transaction)
            if(transaction == null){ //dont create the same transaction if same node for sender and receiver
                transaction = new Transaction(req.body.transaction)
                console.log("create a new transaction **")
            }

            transaction.receiverName = wallet.name,
            transaction.receiverUni = wallet.type == "current" ? wallet.account_u / instance.UDValue : wallet.account_uni,
            wallet.pendingTransactions.push(transaction)
            transaction.save()
            wallet.save()

            /* SEND SOCKET NOTIF TO RECEIVER */
            var uid = instance.name + ":" + wallet.owner._id
            console.log("try emit 'receive-transaction-pending' to", uid)
            req.ws.emit(uid, "receive-transaction-pending", 
                        { senderName: transaction.senderName,
                          amountUni: transaction.amount })
            console.log("** EMITED sock to", uid)
            /* SEND SOCKET NOTIF TO RECEIVER */

            res.json({ wallet:wallet, transaction:transaction })
        }else{
            res.json({  error: true,
                        errorMsg: 'This wallet is not registred on this instance' })
        }
        
    }catch(e){
        console.log("error", e)
        res.json({ error: true, network: false, 
                   errorMsg: 'No instance found in this server' })
    }
})


module.exports = router;