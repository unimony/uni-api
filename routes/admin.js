var express = require("express")
var mongoose = require("mongoose")
var router = express.Router()
var app = express()

const auth = require("../middleware/auth-admin");
const bcrypt = require("bcrypt");

const { User, validate } = require('../models/User')
const Wallet = require('../models/Wallet')
const Instance = require('../models/Instance')
const StateHistory = require('../models/StateHistory')
const Transaction = require('../models/Transaction')

const config = require('config');
const jwt = require('jsonwebtoken');
const core = require('../core/core');


router.get('/instance-info', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/instance-info")
    
    try{
        let instance = await Instance.findOne({ isLocal:true })
        const nbUsers = await User.countDocuments()
        const nbDays = await StateHistory.countDocuments()

        res.json({ instance: instance, 
                    nbUsers: nbUsers, 
                    nbDays: nbDays, 
                    env: config.get('env') });
    }catch(e){
        console.log("error", e)
        res.json({ error: true, instance: null, 
                   errorMsg: 'No instance found in this server' })
    }
})


router.post('/instance-create', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/instance-create")
    try{
        const i = await Instance.findOne({ isLocal:true })
        if(i == null){
            const instance = new Instance({
                name: req.body.name,
                url: config.get('instance_url'),
                port: config.get('instance_port'),
                moneyMassLocal: 0,
                moneyMassGlobal: 0,
                nbUserGlobal: 0,
                UDValue: 0,
                TQRunning: false
            })
            await instance.save();
            res.json({ error: false, instance: instance })
        }else{
            res.json({ error: true, instance: i, 
                       errorMsg: 'An instance allready exists in this server' })
        }
    }catch(e){
        console.log("error", e)
        res.json({ error: true, instance: null })
    }
})


router.post('/add-users-dev'/*, auth*/, async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/instance-init-dev")

    if(config.get("env") != "dev"){
        console.log("error env is not dev")
        res.json({ error: true })
        return
    }


    try{
        const ins = await Instance.findOne({ isLocal:true })
        var nbUsers = req.body.nbUsers
        console.log("let's create", nbUsers, "new users !")

        let nbRealUsers = await User.countDocuments()
        for(var i=1; i<= nbUsers; i++){ 
            //console.log("ready to save user", i, nbUsers)
            //create current wallet (compte courant)
            let firstTrans = ins.moneyMassGlobal / nbRealUsers
            //let index = await Wallet.countDocuments()
            var name = "anonymous" + i
            let wallet = new Wallet({ 
                            uid: '',
                            type: 'current',
                            name: name,
                            isPublic: true,
                            account_u: firstTrans,
                            account_uni: 0,
                            pendingTransactions: [],
                            validatedTransactions: [],
                            created: new Date()  
                        });
            
            //create new user
            var user = new User({
                name: name,
                password: "anonymous",
                email: name + "@mail.com",
                isActive: true,
                isAdmin: false,
                wallets: [ wallet ]
            });

            wallet.uid = ins.name + ":" + wallet.id
            wallet.owner = user.id
            await wallet.save();
            await user.save();

            ins.moneyMassLocal += firstTrans
            ins.moneyMassGlobal = ins.moneyMassLocal
            ins.nbUserGlobal++
            ins.save()

            nbRealUsers++
        }

        console.log(">>> ", nbUsers, "USERS CREATED : OK")
        res.json({ error: false })

    }catch(e){
        console.log("error", e)
        res.json({ error: true, errorMsg: e })
    }
})


router.post('/get-wallets-data', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/get-wallets-data")
    try{
        let walletsCurrent = await Wallet.find({'type': 'current'})
        let walletsCustom = await Wallet.find({'type': 'custom'})
    
        res.json({  error: false, 
                    walletsCurrent: walletsCurrent,
                    walletsCustom: walletsCustom, })
    }catch(e){
        console.log("error", e)
        res.json({ error: true, instance: null })
    }
})


router.post('/calc-money-mass', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/calc-money-mass")
    try{
        let users = await User.find().populate('wallets')
        if(users != null){
            //var mm = 0
            /*users.forEach(user => {
                mm += user.wallets[0].account_u
            });*/
            const i = await Instance.findOne({ isLocal:true })
            //i.moneyMassLocal = mm
            //i.moneyMassGlobal = mm
            i.nbUserGlobal = users.length
            i.UDValue = Instance.calcUniValue(i.moneyMassGlobal, users.length)
            i.save()

            res.json({  error: false, instanceState: i })
        }else{
            res.json({ error: true, 
                       errorMsg: 'No user found in this bdd' })
        }
    }catch(e){
        console.log("error", e)
        res.json({ error: true, instance: null })
    }
})


router.post('/work-ud', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/work-ud")
    try{
        const i = await Instance.findOne({ isLocal: true })
        
        //only for dev
        i.moneyMassGlobal = i.moneyMassLocal
        
        let moneyAdded = 0
        const nbUsers = await User.countDocuments()

        //recalculate UDValue before send new money
        i.UDValue = Instance.calcUniValue(i.moneyMassGlobal, nbUsers)
        var UD = i.UDValue

        console.log("UD", UD)

        //REBASE
        console.log("UD", UD, "need to rebase ?", (i.moneyMassGlobal / nbUsers > 1000), i.moneyMassGlobal / nbUsers, ">", 1000)
        if(i.moneyMassGlobal / nbUsers > 1000){
            console.log("REBASE")
            await User.find().populate('wallets').then(users => {
                users.forEach(user => {
                    //console.log(user.name)
                    user.wallets[0].account_u = user.wallets[0].account_u / 100
                    user.wallets[0].save()
                });
                i.moneyMassLocal = i.moneyMassLocal / 100
                i.moneyMassGlobal = i.moneyMassLocal
                //moneyMass = i.moneyMassGlobal
                i.UDValue = Instance.calcUniValue(i.moneyMassGlobal, nbUsers)
                UD = i.UDValue
            })
        }

        const wbank = await Wallet.findOne({ type: "bank" })
        const average = i.moneyMassGlobal / nbUsers / i.UDValue
        const futurUD = Instance.calcUniValue(i.moneyMassGlobal+(nbUsers*UD), nbUsers)
        //SEND UD
       await User.find().populate('wallets').then(users => { 
            users.forEach(user => {
                 
                user.wallets[0].account_u += UD
                moneyAdded += UD
            

                let transaction = new Transaction({
                    amount: 1,
                    label: "UD",
                    senderUid: wbank.uid,
                    receiverUid: user.wallets[0].uid,
                    senderName: wbank.name,
                    receiverName: user.wallets[0].name,
                    senderUni: 0,
                    receiverUni: user.wallets[0].account_u / futurUD,
                    created: new Date(),
                    validated: new Date(),
                    status: 'validated'
                })

                user.wallets[0].validatedTransactions.push(transaction)
                transaction.save()
                user.wallets[0].save()
                
            });
        })
       
        i.moneyMassLocal += moneyAdded
        //only for dev
        i.moneyMassGlobal = i.moneyMassLocal
        i.UDValue = Instance.calcUniValue(i.moneyMassGlobal, nbUsers)
        
        let date = new Date()
        let nbd = await StateHistory.countDocuments()
        date.setDate(date.getDate() + nbd)
        
        let hist = new StateHistory({
            date: date,
            moneyMassLocal: i.moneyMassLocal,
            moneyMassGlobal: i.moneyMassGlobal,
            nbUsersLocal: nbUsers,
            nbUsersGlobal: nbUsers,
            UDValue: i.UDValue
        })
        hist.save()
        i.save()

        console.log(">>> UDW OK, money added :", moneyAdded, "average", average)
        
        res.json({  error: false, instance: i, nbUsers: nbUsers, date: date })
       
    }catch(e){
        console.log("error", e)
        res.json({ error: true, e: e })
    }

})


router.post('/get-history', auth,  async (req, res) => {
    //const instance_url = config.get('instance_url')
    console.log("-------------------------")
    console.log("/admin/get-history")
    
    const url = config.get('instance_url')
    const port = config.get('instance_port')
    //console.log("url", url, port)
    try{
        const stateHistory = await StateHistory.find();
        console.log("history found", stateHistory.length)
        res.json({  error: false, stateHistory: stateHistory });
    }catch(e){
        console.log("error", e)
        res.json({ error: true, 
                errorMsg: 'No history found in this server' })
    }
})


router.get('/update-network', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/get-network")
    
    try{
        //const network = await Instance.find().select("-nodeKey")
        let network = []
        core.broadcaster.get("/broadcast/instance-info",
            function(result){ console.log("res from /broadcast/instance-info", result)
                if(result != null){
                    network = result
                    //here can update local instance data
                    res.json({ error: false, network: network });
                }else{
                    console.log("error 1 on /broadcast/instance-info")
                }
            },
            function(res){
                console.log("error 2 on /broadcast/instance-info")
            },
            'instance'
        )
        //console.log(network)
        //res.json({ network: network });
    }catch(e){
        console.log("error", e)
        res.json({ error: true, network: false, 
                   errorMsg: 'No instance found in this server' })
    }
})

module.exports = router;