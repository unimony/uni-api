var express = require("express")
var mongoose = require("mongoose")
var cors = require('cors')
const config = require("config");

var app = express()

//console.log("port", process.argv[process.argv.length-1])

var port = config.get('instance_port') //3000
if(process.argv[process.argv.length-4] == "--port") 
    port = process.argv[process.argv.length-3]

var sockPort = config.get('instance_port_socket') //8080
if(process.argv[process.argv.length-2] == "--sock") 
    sockPort = process.argv[process.argv.length-1]
    
var mongodbname = 'uni'+port
mongoose.connect('mongodb://localhost:27017/'+mongodbname, {useNewUrlParser: true, useUnifiedTopology:true});

require('./models/User');
require('./models/Wallet');
require('./models/Instance');

//const AllowOrigin = "http://192.168.1.69:8080"
const AllowOrigin = "*" // "http://localhost:8080"

//init socket
const http = require("http");
var server = http.createServer().listen(sockPort)
var io = require('socket.io').listen(server);

let ws = require('./core/socket');
ws.init(io)

//add x-tocken & socket to request headers
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", AllowOrigin); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "X-Auth-Token, Origin, X-Requested-With, Content-Type, Accept");
    req.ws = ws;
    //console.log("***************** WS", ws.clients)
    next();
});

//use config module to get the privatekey, if no private key set, end the application
if (!config.get("access_pk")) {
    console.error("FATAL ERROR: access_pk is not defined.");
    process.exit(1);
}

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


app.use('/user', require('./routes/user'))
app.use('/auth', require('./routes/auth'))
app.use('/admin', require('./routes/admin'))
app.use('/instance', require('./routes/instance'))
app.use('/tqw', require('./routes/tqw'))
app.use('/broadcast', require('./routes/broadcast'))

app.listen(port)


console.log("API started on port", port, "mongodbname", mongodbname)
console.log("SOCKET started on port", sockPort)